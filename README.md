This is a fork of https://github.com/joswr1ght/kraken.

The following issues are fixed:

 * Fixed output of kraken and input of find_kc (now correctly in HEX)
 * Working with python 2 and 3